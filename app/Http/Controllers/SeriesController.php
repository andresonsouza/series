<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

class SeriesController extends Controller
{
    public function index(Request $request)
    {

        $series = [
            'Dirk Gently',
            'Sherlock Holmes',
            'Stranger Things'
        ];

        // return view('series.index', [
        //     'series' => $series
        // ]);

        return view('series.index', compact('series'));
    }

    public function create()
    {
        return view('series.create');
    }
}
